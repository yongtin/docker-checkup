#!/bin/bash
#
# wish this can be docker-compose but docker-compose build and up modes does not work
# 
docker build -t app-builder ./builder
docker run -td --name app-artifact app-builder /bin/sh
docker cp app-artifact:/go/bin/checkup ./artifacts/
docker rm -f app-artifact
docker rmi app-builder
docker build -t registry.gitlab.com/yongtin/docker-checkup .
