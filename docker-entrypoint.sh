#!/bin/bash

if [ -f /etc/checkup/checkers.json ] && [ -n ${STORAGE_GITHUB_REPO_OWNER} ] && [ -n ${STORAGE_GITHUB_REPO_NAME} ] && [ -n ${STORAGE_GITHUB_ACCESS_TOKEN} ]; then

cat << CHECKUP_JSON_CONFIG > /checkup.json
{
  $(cat /etc/checkup/checkers.json), 
  "storage": {
    "provider": "github",
    "access_token": "$STORAGE_GITHUB_ACCESS_TOKEN",
    "repository_owner": "$STORAGE_GITHUB_REPO_OWNER",
    "repository_name": "$STORAGE_GITHUB_REPO_NAME",
    "committer_name": "checkup_bot",
    "committer_email": "checkup_bot@nowhere",
    "branch": "master",
    "dir": "data"
  }
}
CHECKUP_JSON_CONFIG

fi

/checkup provision

exec "$@"

