FROM ubuntu:16.04

RUN apt-get update && apt-get -y install curl && rm -rf /var/lib/apt/lists/*
ADD checkup.cron /checkup.cron
RUN chmod +x /checkup.cron

#ADD https://github.com/krallin/tini/releases/download/v0.15.0/tini_0.15.0.deb /opt/tini_0.15.0.deb
#RUN dpkg -i /opt/tini_0.15.0.deb
#RUN /bin/rm -f /opt/tini_0.15.0.deb

RUN mkdir /etc/checkup
ADD checkup.json /checkup.json 

ADD artifacts/checkup /checkup
RUN chmod +x /checkup

ADD docker-entrypoint.sh /docker-entrypoint.sh
RUN chmod +x /docker-entrypoint.sh

ENTRYPOINT ["/docker-entrypoint.sh"]
CMD ["/checkup.cron"]
